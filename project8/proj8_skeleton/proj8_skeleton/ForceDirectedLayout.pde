
class ForceDirectedLayout extends Frame {
  
  
  float RESTING_LENGTH = 10.0f;   // update this value
  float SPRING_SCALE   = 0.0075f; // update this value
  float REPULSE_SCALE  = 400f;  // update this value

  float TIME_STEP      = 0.5f;    // probably don't need to update this

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;
 
 

  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  GraphVertex selected = null;
  

  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;
  }

  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A REPULSIVE FORCE
    
    // v0.addForce( 1,1 );
     //v1.addForce( 1,1 );
    float dx=v0.getPosition().x-v1.getPosition().x;
    float dy=v0.pos.y-v1.pos.y;
    float dis=sqrt(dx*dx+dy*dy);
    float x1=dx/dis;
    float y1=dy/dis;
    float forcefx=x1*v0.mass*v1.mass*REPULSE_SCALE/(dis*dis);
    float forcefy=y1*v0.mass*v1.mass*REPULSE_SCALE/(dis*dis);
    //println(forcefx);
    //println(forcefy);
    
   
    
    v0.addForce(forcefx,forcefy);
    v1.addForce(-forcefx,-forcefy);
      
    
    
  }

  void applySpringForce( GraphEdge edge ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A SPRING FORCE
    
    // edge.v0.addForce( ... );
    // edge.v1.addForce( ... );
   float dx=edge.v0.pos.x-edge.v1.pos.x;
    float dy=edge.v0.pos.y-edge.v1.pos.y;
    float dis=sqrt(dx*dx+dy*dy);
    float x1=dx/dis;
    float y1=dy/dis;
    float d=dis-RESTING_LENGTH;
    float springfx=-1*SPRING_SCALE*d*x1;
    float springfy=-1*SPRING_SCALE*d*y1;
    
     edge.v0.addForce( springfx,springfy);
     edge.v1.addForce( -springfx,-springfy);
     
  }

  void draw() {
    textSize(32);
    fill(92,66,244);
    text("Force Directed Graph",200,25);
    textSize(10);
    fill(0);
    strokeWeight(2);
    //ellipse(7,35,7,7);
    text("Hover over the vertex to get the info",25,60);
    stroke(100,100,100);
    strokeWeight(1);
    line(600,50,600,60);
    strokeWeight(2);
    line(620,50,620,60);
    strokeWeight(3);
    line(640,50,640,60);
    strokeWeight(4);
    line(660,50,660,60);
    strokeWeight(5);
    line(680,50,680,60);
    fill(0);
    text("Edge Weight",610,40);
    fill(92,66,244);
    text("Min",580,60);
    text("Max",690,60);
    update(); // don't modify this line
    
    // TODO: ADD CODE TO DRAW THE GRAPH
    //drawnodes();
        for(int i=0;i<edges.size();i++)
    {
      if(edges.get(i).weight==1)strokeWeight(1);
      if(edges.get(i).weight==2)strokeWeight(2);
      if(edges.get(i).weight==3)strokeWeight(3);
      if(edges.get(i).weight==4)strokeWeight(4);
      if(edges.get(i).weight==5)strokeWeight(5);
      stroke(100,100,100);
     //fill(255,0,0);
    line(edges.get(i).v0.pos.x,edges.get(i).v0.pos.y,edges.get(i).v1.pos.x,edges.get(i).v1.pos.y);
  }
    for(int i=0;i<verts.size();i++)
  {
    stroke(0);
   // println("no");
   if(verts.get(i).group==1)
   {
     fill(255,0,0);
   }
    if(verts.get(i).group==2)
   {
     fill(0,255,0);
   }
    if(verts.get(i).group==3)
   {
     fill(0,0,255);
   }
    if(verts.get(i).group==4)
   {
     fill(255,255,0);
   }
    if(verts.get(i).group==5)
   {
     fill(255,0,255);
   }
   if(verts.get(i).group==6)
   {
     fill(0,0,0);
   }
   if(verts.get(i).group==7)
   {
     fill(100,100,100);
   }
   if(verts.get(i).group==8)
   {
     fill(0,120,120);
   }
   if(verts.get(i).group==9)
   {
     fill(0,20,190);
   }
   if(verts.get(i).group==10)
   {
     fill(0,255,255);
   }
   
   ellipse(verts.get(i).pos.x,verts.get(i).pos.y,15,15);
  // println(verts.get(i).pos.x);
    //  println(verts.get(i).pos.y);

   fill(255,0,0);
   ellipse(700,100,15,15);
   textSize(15);
   fill(0);
   text("group-1",720,105);
   fill(0,255,0);
   ellipse(700,120,15,15);
   fill(0);
   text("group-2",720,125);
   fill(0,0,255);
   ellipse(700,140,15,15);
   fill(0);
   text("group-3",720,145);
   
   fill(255,255,0);
   ellipse(700,160,15,15);
   fill(0);
   text("group-4",720,165);
   
   fill(255,0,255);
   ellipse(700,180,15,15);
   fill(0);
   text("group-5",720,185);
   
   fill(0,0,0);
   ellipse(700,200,15,15);
   fill(0);
   text("group-6",720,205);
 
   fill(100,100,100);
   ellipse(700,220,15,15);
   fill(0);
   text("group-7",720,225);
   
   fill(0,120,120);
   ellipse(700,240,15,15);
   fill(0);
   text("group-8",720,245);
   
   fill(0,20,190);
   ellipse(700,260,15,15);
   fill(0);
   text("group-9",720,265);
   
   fill(0,255,255);
   ellipse(700,280,15,15);
   fill(0);
   text("group-10",720,285);

   
   
   
    
  }
    //drawedges();
  
    
  }
void drawnodes()
{
  for(int i=0;i<verts.size();i++)
  {
    stroke(0);
   // println("no");
   if(verts.get(i).group==1)
   {
     fill(255,0,0);
   }
    if(verts.get(i).group==2)
   {
     fill(0,255,0);
   }
    if(verts.get(i).group==3)
   {
     fill(0,0,255);
   }
    if(verts.get(i).group==4)
   {
     fill(255,255,0);
   }
    if(verts.get(i).group==5)
   {
     fill(255,0,255);
   }
    if(verts.get(i).group==6)
   {
     fill(255,0,255);
   } if(verts.get(i).group==5)
   {
     fill(255,0,255);
   } if(verts.get(i).group==5)
   {
     fill(255,0,255);
   } if(verts.get(i).group==5)
   {
     fill(255,0,255);
   } if(verts.get(i).group==5)
   {
     fill(255,0,255);
   }
   ellipse(verts.get(i).pos.x,verts.get(i).pos.y,15,15);
  // println(verts.get(i).pos.x);
    //  println(verts.get(i).pos.y);

   
    //println("yes");
    
  }
  
}
void drawedges()
{
  for(int i=0;i<edges.size();i++)
  {
    fill(0);
    line(edges.get(i).v0.pos.x,edges.get(i).v0.pos.y,edges.get(i).v1.pos.x,edges.get(i).v1.pos.y);
  }
}

  void mousePressed() { 
    // TODO: ADD SOME INTERACTION CODE
    for(GraphVertex v:verts)
    {
      if(v.mouseInside())
      {
        println("shiva");
        text("pressed",v.pos.x,v.pos.y);
        
      }
    }

  }
  void mouseover()
  {
    for(GraphVertex v:verts)
    {
      if(v.mouseInside())
      {
       // println("shiva");
        //text("pressed",v.pos.x,v.pos.y);
    if(v.group==1)
   {
     fill(255,0,0);
   }
    if(v.group==2)
   {
     fill(0,255,0);
   }
    if(v.group==3)
   {
     fill(0,0,255);
   }
    if(v.group==4)
   {
     fill(255,255,0);
   }
    if(v.group==5)
   {
     fill(255,0,255);
   }
   if(v.group==6)
   {
     fill(0,0,0);
   }
   if(v.group==7)
   {
     fill(100,100,100);
   }
   if(v.group==8)
   {
     fill(0,120,120);
   }
   if(v.group==9)
   {
     fill(0,20,190);
   }
   if(v.group==10)
   {
     fill(0,255,255);
   }
   
        ellipse(v.pos.x,v.pos.y,40,40);
        stroke(0);
        fill(0);
        textSize(15);
        text(v.id,v.pos.x,v.pos.y);
        text("group -"+v.group,v.pos.x,v.pos.y+15);
      }
    }
    
  }

  void mouseReleased() {    
    // TODO: ADD SOME INTERACTION CODE

  }



  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}