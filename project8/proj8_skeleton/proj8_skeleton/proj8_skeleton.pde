JSONObject response;
Frame myFrame = null;

void setup() {
  size(800, 800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();
    ArrayList<String> vertvalue=new ArrayList<String>();


    // TODO: PUT CODE IN TO LOAD THE GRAPH    
      response = loadJSONObject(selection.getAbsolutePath());
        JSONArray nodes = response.getJSONArray("nodes");
        JSONArray links=response.getJSONArray("links");
        for(int i=0;i<nodes.size();i++)
        {
          JSONObject obj=nodes.getJSONObject(i);
          String id=obj.getString("id");
          vertvalue.add(id);
          int grp=obj.getInt("group");
          //float x=(float)random(0,800);
       //   println(x);
          //float y=(float)(random(0,800));
         // println(y);
          
          verts.add(new GraphVertex(id,grp,random(300,500),random(300,500)));
        }
        for(int i=0;i<links.size();i++)
        {
          JSONObject obj=links.getJSONObject(i);
          String source=obj.getString("source");
          String target=obj.getString("target");
          int value=obj.getInt("value");
          
          
          int indexsrc=vertvalue.indexOf(source);
          int indextar=vertvalue.indexOf(target);
          
          edges.add(new GraphEdge(verts.get(indexsrc),verts.get(indextar),value));
       // System.out.println(edges.get(i).v0.id);
        //System.out.println(edges.get(i).v0.id);
        }

    myFrame = new ForceDirectedLayout( verts, edges );
  }
}


void draw() {
  background( 255 );

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
    myFrame.mouseover();
    
    
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 10;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  void mousePressed() { }
  void mouseReleased() { }
  void mouseover(){}
  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
  
}