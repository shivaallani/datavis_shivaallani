Table data;
int indic=0;
int margin=100;
float satm;
float satv;
float gpa;
float act;
float satmarr[];
float satvarr[];
float gpaarr[];
float actarr[];
float satmmin;
float satmmax;
float satvmin;
float satvmax;
float actmax;
float actmin;
float gpamax;
float gpamin;
int n=0;
File file;
void setup()
{
  size(800,400);
   selectInput("Select a File to process:","fileSelected");
   fill(0, 102, 153);
   text("click on right arrow (-->) and left arrow(<--) to change the axis",width/3,height-75);
    
   
}

void fileSelected(File selection)
{
  
  if(selection==null)
  {
    println("user closed the window");
  }
  else
  {
    data = loadTable(selection.getAbsolutePath(), "header");
 int n=data.getRowCount();
 println(n);
 satmarr=new float[data.getRowCount()];
 satvarr=new float[data.getRowCount()];
 gpaarr=new float[data.getRowCount()];
 actarr=new float[data.getRowCount()];
      
  }
  
}

void draw()
{
  background(255);
   fill(0, 102, 153);
   text("click on right arrow (-->) and left arrow(<--) to change the axis",width/4,height-75);
   text("minimum",0,height-margin);
   text("maximum",0,margin);
  while(data==null)
   {
     delay(1000);
   }
   //textSize(32);
   
   textSize(13);
  
  stroke(0);
 noFill();
 fill(0,0,0,1);
   text("click",1,height-75);
 stroke(255,0,255,100);
 
  calc_min_max();
  drawaxis();
  drawgraph();
  
  noFill();
  
}

void drawgraph()
{
if(indic==0)
{
  for(int j=0;j<data.getRowCount();j++)
  {
   float x1=map(satmarr[j],satmmin,satmmax,margin,height-margin);
   float x2=map(satvarr[j],satvmin,satvmax,margin,height-margin);
   float x3=map(actarr[j],actmin,actmax,margin,height-margin);
   float x4=map(gpaarr[j],gpamin,gpamax,margin,height-margin);
   line(margin,x1,margin+(width-2*margin)/3,x2);
   line(margin+(width-2*margin)/3,x2,margin+2*(width-2*margin)/3,x3);
   line(margin+2*(width-2*margin)/3,x3,width-margin,x4);
   text(data.getColumnTitle(0),margin-10,margin-30);
   text(satmmax,margin-10,margin);
   text(satmmin,margin-10,height-margin+10);
   text(data.getColumnTitle(1),margin+((width-2*margin)/3)-10,margin-30);
   text(satvmax,margin+((width-2*margin)/3)-10,margin);
   text(satvmin,margin+((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(2),margin+2*((width-2*margin)/3)-10,margin-30);
   text(actmax,margin+2*((width-2*margin)/3)-10,margin);
   text(actmin,margin+2*((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(3),width-margin-10,margin-30);
   text(gpamax,width-margin-10,margin);
   text(gpamin,width-margin-10,height-margin+10);
   
  if(mouseX >margin && mouseX <margin+(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(0)+" is"+map(mouseY,margin,height-margin,satmmax,satmmin),15,height-25);
     noFill();
   } 
   if(mouseX >margin+(width-2*margin)/3 && mouseX < margin+2*(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(1)+" is"+map(mouseY,margin,height-margin,satvmin,satvmax),15,height-25);
     noFill();
   } 
    if(mouseX >margin+2*(width-2*margin)/3 && mouseX < width-margin && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(2)+" is"+map(mouseY,margin,height-margin,actmin,actmax),15,height-25);
     noFill();
   } 
   
   
  }
  
}
 if(indic==1)
 {
   for(int j=0;j<data.getRowCount();j++)
  {
   float x1=map(satvarr[j],satvmin,satvmax,margin,height-margin);
   float x2=map(satmarr[j],satmmin,satmmax,margin,height-margin);
   float x3=map(actarr[j],actmin,actmax,margin,height-margin);
   float x4=map(gpaarr[j],gpamin,gpamax,margin,height-margin);
   line(margin,x1,margin+(width-2*margin)/3,x2);
   line(margin+(width-2*margin)/3,x2,margin+2*(width-2*margin)/3,x3);
   line(margin+2*(width-2*margin)/3,x3,width-margin,x4);
   text(data.getColumnTitle(1),margin-10,margin-30);
   text(satvmax,margin-10,margin);
   text(satvmin,margin-10,height-margin+10);
   text(data.getColumnTitle(0),margin+((width-2*margin)/3)-10,margin-30);
   text(satmmax,margin+((width-2*margin)/3)-10,margin);
   text(satmmin,margin+((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(2),margin+2*((width-2*margin)/3)-10,margin-30);
   text(actmax,margin+2*((width-2*margin)/3)-10,margin);
   text(actmin,margin+2*((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(3),width-margin-10,margin-30);
   text(gpamax,width-margin-10,margin);
   text(gpamin,width-margin-10,height-margin+10);
     if(mouseX >margin && mouseX <margin+(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(1)+" is"+map(mouseY,margin,height-margin,satvmin,satvmax),15,height-25);
     noFill();
   } 
   if(mouseX >margin+(width-2*margin)/3 && mouseX < margin+2*(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(0)+" is"+map(mouseY,margin,height-margin,satmmax,satmmin),15,height-25);
     noFill();
   } 
    if(mouseX >margin+2*(width-2*margin)/3 && mouseX < width-margin && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(2)+" is"+map(mouseY,margin,height-margin,actmin,actmax),15,height-25);
     noFill();
   } 
  }

   
 }
 if(indic==2)
 {
   for(int j=0;j<data.getRowCount();j++)
  {
   float x1=map(satvarr[j],satvmin,satvmax,margin,height-margin);
   float x2=map(actarr[j],actmin,actmax,margin,height-margin);
   float x3=map(satmarr[j],satmmin,satmmax,margin,height-margin);
   float x4=map(gpaarr[j],gpamin,gpamax,margin,height-margin);
   line(margin,x1,margin+(width-2*margin)/3,x2);
   line(margin+(width-2*margin)/3,x2,margin+2*(width-2*margin)/3,x3);
   line(margin+2*(width-2*margin)/3,x3,width-margin,x4);
   text(data.getColumnTitle(1),margin-10,margin-30);
   text(satvmax,margin-10,margin);
   text(satvmin,margin-10,height-margin+10);
   text(data.getColumnTitle(2),margin+((width-2*margin)/3)-10,margin-30);
   text(actmax,margin+((width-2*margin)/3)-10,margin);
   text(actmin,margin+((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(0),margin+2*((width-2*margin)/3)-10,margin-30);
   text(satmmax,margin+2*((width-2*margin)/3)-10,margin);
   text(satmmin,margin+2*((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(3),width-margin-10,margin-30);
   text(gpamax,width-margin-10,margin);
   text(gpamin,width-margin-10,height-margin+10);
    if(mouseX >margin && mouseX <margin+(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(1)+" is"+map(mouseY,margin,height-margin,satvmin,satvmax),15,height-25);
     noFill();
   } 
   if(mouseX >margin+(width-2*margin)/3 && mouseX < margin+2*(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(2)+" is"+map(mouseY,margin,height-margin,actmin,actmax),15,height-25);
     noFill();
   } 
    if(mouseX >margin+2*(width-2*margin)/3 && mouseX < width-margin && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(0)+" is"+map(mouseY,margin,height-margin,satmmax,satmmin),15,height-25);
     noFill();
   } 
  }
   
 }
 if(indic==3)
 {
   for(int j=0;j<data.getRowCount();j++)
  {
   float x1=map(satvarr[j],satvmin,satvmax,margin,height-margin);
   float x2=map(actarr[j],actmin,actmax,margin,height-margin);
   float x3=map(gpaarr[j],gpamin,gpamax,margin,height-margin);
   float x4=map(satmarr[j],satmmin,satmmax,margin,height-margin);
   line(margin,x1,margin+(width-2*margin)/3,x2);
   line(margin+(width-2*margin)/3,x2,margin+2*(width-2*margin)/3,x3);
   line(margin+2*(width-2*margin)/3,x3,width-margin,x4);
   text(data.getColumnTitle(1),margin-10,margin-30);
   text(satvmax,margin-10,margin);
   text(satvmin,margin-10,height-margin+10);
   text(data.getColumnTitle(2),margin+((width-2*margin)/3)-10,margin-30);
   text(actmax,margin+((width-2*margin)/3)-10,margin);
   text(actmin,margin+((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(3),margin+2*((width-2*margin)/3)-10,margin-30);
   text(gpamax,margin+2*((width-2*margin)/3)-10,margin);
   text(gpamin,margin+2*((width-2*margin)/3)-10,height-margin+10);
   text(data.getColumnTitle(0),width-margin-10,margin-30);
   text(satmmax,width-margin-10,margin);
   text(satmmin,width-margin-10,height-margin+10);
       if(mouseX >margin && mouseX <margin+(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(1)+" is"+map(mouseY,margin,height-margin,satvmin,satvmax),15,height-25);
     noFill();
   } 
   if(mouseX >margin+(width-2*margin)/3 && mouseX < margin+2*(width-2*margin)/3 && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(2)+" is"+map(mouseY,margin,height-margin,actmin,actmax),15,height-25);
     noFill();
   } 
    if(mouseX >margin+2*(width-2*margin)/3 && mouseX < width-margin && mouseY>margin && mouseY<height-margin)
   {
fill(0,0,0,1);
text("avg of the student with respect to "+data.getColumnTitle(3)+" is"+map(mouseY,margin,height-margin,gpamin,gpamax),15,height-25);
     noFill();
   } 
  }
 }
   
}  
  

void calc_min_max()
{
  for(int j=0;j<data.getRowCount();j++)
  {
  
    TableRow row=data.getRow(j);
    satmarr[j]=row.getFloat(0);
    satvarr[j]=row.getFloat(1);
    actarr[j]=row.getFloat(2);
    gpaarr[j]=row.getFloat(3);
    
  }
  
  satvmin=min(satvarr);
  satvmax=max(satvarr);
  satmmin=min(satmarr);
  //println(satmmin);
  //println(satmmax);
  satmmax=max(satmarr);
  actmin=min(actarr);
  actmax=max(actarr);
  gpamax=max(gpaarr);
  gpamin=min(gpaarr);
  
  
}

void drawaxis()
{
  line(margin,margin,margin,height-margin);
  line(margin+(width-2*margin)/3,margin,margin+(width-2*margin)/3,height-margin);
  line(margin+(2*(width-2*margin)/3),margin,margin+(2*(width-2*margin)/3),height-margin);
  line(width-margin,margin,width-margin,height-margin);
}

void keyPressed() {
  
  if(key==CODED)
  {
    if(keyCode==LEFT)
    {
      if(indic!=0)
      {
        indic--;
        println(indic);
        
      }
    }
    if(keyCode==RIGHT)
    {
      if(indic!=3)
      {
      
      indic++;
      println(indic);
      }
    }
  }
  
 
}