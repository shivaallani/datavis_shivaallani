Table table;
float SATM,SATV,ACT,GPA;
int margin,i;
void setup()
{
  size(600,600);
  background(127);
  selectInput("Select a file to process:", "fileSelected");
  while(table==null)
  {delay(1000);
  }
  
  noLoop();
}
public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  }
  else
  {
    table = loadTable(selection.getAbsolutePath(), "header");
    
  }
}

void draw()
{
  drawaxis();
  drawplot();
  drawlabels();
}
void drawaxis()
{
  line(100,500,100,100);
    line(500,500,500,100);
    line(200,500,200,100);
    line(100,500,500,500);
    line(400,500,400,100);
    line(100,400,500,400);
    line(100,300,500,300);
    line(100,200,500,200);
    line(100,100,500,100);
    line(300,500,300,100);
}
void drawplot()
{
  for(TableRow row : table.rows())
  {
    
    SATM=row.getFloat(0);
    GPA=row.getFloat(3);
    SATV=row.getFloat(1);
    ACT=row.getFloat(2);
    
    ellipse(100+(ACT*2),500-(GPA*10),3,3);
    ellipse(100+(ACT*2),400-(SATV/10),3,3);
    ellipse(100+(ACT*2),300-(SATM/10),3,3);
    
    ellipse(200+(SATM/10),500-(GPA*10),3,3);
    ellipse(200+(SATM/10),400-(SATV/10),3,3);
    ellipse(200+(SATM/10),200-(ACT*2),3,3);
    
    
    
    ellipse(300+(SATV/10),500-(GPA*10),3,3);
    ellipse(300+(SATV/10),300-(SATM/10),3,3);
    ellipse(300+(SATV/10),200-(ACT*2),3,3);

    ellipse(400+(GPA*10),400-(SATV/10),3,3);
    ellipse(400+(GPA*10),300-(SATM/10),3,3);
    ellipse(400+(GPA*10),200-(ACT*2),3,3);
    
  }
  
}
void drawlabels()
{
  text(table.getColumnTitle(2),50,150);
  text(table.getColumnTitle(0),50,250);
  text(table.getColumnTitle(1),50,350);
  text(table.getColumnTitle(3),50,450);
  text(table.getColumnTitle(2),150,530);
  text(table.getColumnTitle(0),250,530);
  text(table.getColumnTitle(1),350,530);
  text(table.getColumnTitle(3),450,530);
  textSize(32);
  text(table.getColumnTitle(0)+" vs "+ table.getColumnTitle(1)+" vs " + table.getColumnTitle(2) + " vs " + table.getColumnTitle(3),100,50);
  
  
}