Table table;
float[] xaxisdata;
float[] yaxisdata;
float n;
int i=0,j=0;
int margin,graphheight;
float xspacer;
PVector[] positions;
float xaxismin;
float xaxismax;
float yaxismin;
float yaxismax;
String xaxistitle;
String yaxistitle;




void setup()
{
   //setup background and size.
    size(600,600);
   
    selectInput("Please select a csv: ", "fileSelected");
    while(table==null)
    {
      delay(1000);
    }
  
}
void draw()
{
   for (i=0;i<table.getRowCount();i++) 
   {
     //fill(255);
   ellipse(positions[i].x,positions[i].y,3,3);
   }  
     drawlabels();
   drawaxis();
 
  
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else { // get the table 
    
    table = loadTable(selection.getAbsolutePath(), "header");
    

    int n=table.getRowCount();
    yaxisdata=new float[n];
    xaxisdata=new float[n];
    positions=new PVector[n];

    loaddata();
  }
}
void loaddata()
{// load data and initialized the variables
   for (TableRow row : table.rows()) {

  xaxisdata[i]=row.getFloat(0);
  yaxisdata[i]=row.getFloat(1);
  
    i++;
  }
   margin=100;
graphheight=(height-margin)-margin;
//xspacer=(width-margin-margin)/(table.getRowCount()-1);
xaxismin= min(xaxisdata);
xaxismax=max(xaxisdata);
yaxismax=max(yaxisdata);
yaxismin=min(yaxisdata);


for(int r=0;r<table.getRowCount();r++)
{
  float yadjscore=map(yaxisdata[r],yaxismin,yaxismax,0,graphheight);
  float ypos=height-margin-yadjscore;
  float xadjscore=map(xaxisdata[r],xaxismin,xaxismax,0,width-margin-margin);
  //println(xadjscore);
  float xpos=margin+xadjscore;
  positions[r]=new PVector(xpos,ypos);
}
}

void drawaxis()
{ //draw X and Y axis
  stroke(0);
  line(margin,height-margin,width-margin,height-margin);
  line(margin,height-margin,margin,margin);
 
}
void drawlabels()
{  
  int c=(height-2*margin)/10;
  int d=(width-2*margin)/10;
  float xnormfactor=(xaxismax-xaxismin)/10;
  float ynormfactor=(yaxismax-yaxismin)/10;
  for(int j=0;j<10;j++)
  // labeling for x axis
  {
    noStroke();
fill(0,126,255);
    textSize(10);
    String S=String.format("%.2f",yaxismin+(ynormfactor*j));
    text(S,margin-50,(height-margin)-(c*j));
   stroke(100);
    line(margin,(height-margin)-(c*j),width-margin,(height-margin)-(c*j));
  }
  //labeling for y axis
for(int k=0;k<10;k++)
{  noStroke();
 fill(0,126,255);
     String L=String.format("%.2f",xaxismin+(xnormfactor*k));
   text(L,margin+(d*k),(height-margin+25));
   stroke(100);
   line(margin+(d*k),height-margin,margin+(d*k),margin);
   
}
//drawing head title
fill(127,0,0);
textSize(32);
xaxistitle=table.getColumnTitle(0);
yaxistitle=table.getColumnTitle(1);
text(xaxistitle +"  Vs  "+yaxistitle,width/4,margin/2);
textSize(15);
text(xaxistitle,width/2,550);
text(yaxistitle,15,height/2+25);
  
}