class Scatterplotzoom extends Frame
{
  Scatterplot sp;
  Table data;
  Scatterplotzoom(Table data,Scatterplot sp)
  {
    this.sp=sp;
    this.data=data;
  }
  
  void draw()
  { 
        
    for( int i = 0; i < table.getRowCount(); i++ ){
        TableRow r = table.getRow(i);
        
        float x = map( r.getFloat(sp.idx0), sp.minX, sp.maxX, u0, u0+w );
        float y = map( r.getFloat(sp.idx1),sp.minY,sp. maxY, v0+h, v0 );
        
       // stroke( 0 );
       // fill(255,0,0);
       // stroke(204, 102, 0);
        stroke(255, 0, 102);
        ellipse( x,y,3,3 );
     }
    
    
  }
  void setPosition(float u0, float v0, float w, float h)
  {
    super.setPosition(u0,v0,w,h);
  }
  
}