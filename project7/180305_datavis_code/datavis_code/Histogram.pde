class Histogram extends Frame
{
  Table data;
  ArrayList<Integer> useColumns= new ArrayList<Integer>();
  int n;
  int k=5; 
  ArrayList<ArrayList <Integer>> list = new ArrayList<ArrayList <Integer>>();
  int Barx=0;
  int bin;
  int temp=100;
  float barwidth;
  float barspace;
  float border=25;
  float baru0;
  ArrayList<Histobar> axes = new ArrayList<Histobar>();

  
  Histogram(Table data, ArrayList<Integer> useColumns)
  {
    this.data=data;
    this.useColumns=useColumns;
    n=data.getRowCount();
  
 
  }
   void keyPressed()
  {
    
    if (key == CODED) {
    if (keyCode == RIGHT) {
      
     if(Barx<useColumns.size()-1)
     {
      Barx++;
     }
     else{
     }
    }  if (keyCode == LEFT) {
      if(Barx>0)
      {
        
      Barx--;
    }
      else
      {
      }
  } 
  if(keyCode == UP)
  {
    if(k==8)
    {
    }
    else{
    k++;
    }
  }
  if(keyCode == DOWN)
  {
    if(k==2)
    {
      
    }
    else{
    k--;
    }
  }
  }
  }
  
  void setPosition(float u0, float v0, float w, float h)
  {
    super.setPosition(u0,v0,w,h);
  }
  
  
  
  
  
  void draw()
  {
    loaddata();
    drawscale();
    textSize(25);
    text("HISTOGRAM",u0+250,v0+10);
    textSize(10);
    text("Press Right/left to change the axis and UP/DOWN to change bins on the keyboard",u0+w+25,v0+50);
  }
  void drawscale()
  {
    stroke(0);
    textSize(10);
    line(u0,v0+100,u0,v0+h-2*border);
    line(u0,v0+h-2*border,u0+w,v0+h-2*border);
    for(int i=0;i<=10;i++)
    {
      text(i*20,u0,v0+h-2*border-i*20);
    }
      fill(192,0,100);

    rect(u0+w/2-100,v0+h-border,10,10);
    textSize(20);
    
    text(table.getColumnTitle(Barx),u0+w/2,v0+h-0.5*border);
  }
  void loaddata()
  {
    list.clear();
    axes.clear();
    barwidth=(w/k)*0.6;
    barspace=(w/k)*0.4;
    for(int i=0;i<=k;i++)
    {
      list.add(new ArrayList<Integer> ());
      
    }
    float maxv=max(data.getFloatColumn(Barx));
    float minv=min(data.getFloatColumn(Barx));
 for(int i=0;i<n; i++)
 {
   float val=data.getFloat(i,Barx);
   bin=floor(k*(val-minv)/(maxv-minv));
   
  // println(i+ "bin="+bin);
   if(bin==k)
   {
     bin=bin-1;
   }
   list.get(bin).add((int)val);
   
 }
 baru0=u0;
 for(int i=0;i<k;i++)
 {
  baru0=baru0+barwidth;
 Histobar bar=  new Histobar(list.get(i),baru0, h-list.get(i).size(),barwidth , list.get(i).size());
  // System.out.println(list.get(i).size());
  axes.add(bar);
  baru0=baru0+barspace;
  fill(192,0,100);
rect(bar.u0,bar.v0,bar.w,bar.h);   
textSize(10);
text(bar.num.size(),bar.u0+25,bar.v0-10);
fill(0);
text(axes.indexOf(bar)+1,bar.u0+25,v0+h-25);

 }
    
  }
  void mouseover()
  {
     for( Histobar b :axes ){
       if( b.mouseInside() ){
       
        //println(b.num.size());
        textSize(15);
        fill(100,100,100);
      rect(b.u0,b.v0,b.w,b.h);
        fill(0);
        fill(192,0,100);
        
        rect(u0+w-75,v0+10,100,100);
        textSize(13);
        fill(0);
        text("mean = "+b.mean,u0+w+10,v0+30);
        text("std dev="+b.std,u0+w+15,v0+50);
        
       }
    }
  }
}

class Histobar extends Frame
{
  ArrayList<Integer> num;
  int temp;
  int mean;
  float temp2;
  float std;
  int n;
  
  Histobar(ArrayList<Integer> num,float u0,float v0,float w,float h)
  {
    this.num=num;
    this.u0=u0;
    this.v0=v0;
    this.w=w;
    this.h=h;
    calculatemean();
    calculatestd();
    
    
  }
  void calculatemean()
  {
    for(int i=0;i<num.size();i++)
    {
      temp=temp+num.get(i);
      
    }
    
    mean=temp/num.size();
  }
  void  calculatestd()
  {
    for(int i=0;i<num.size();i++)
    {
      temp2=num.get(i)-mean+temp2;
      
    }
    std=temp2;
  }

  
  void draw()
  {
  }
  
}