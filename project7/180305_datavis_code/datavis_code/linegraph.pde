class Linegraph extends Frame{
  Table data;
    float border=75;
    int Barx=0;
    float wid=w-20-20;
    float hei=h-20-20;
    float space;
    int j; 
    float barwidth;
    float n;
    float baru0;
    float barv0;
    int temp=0;
    ArrayList<Integer>useColumns=new ArrayList<Integer>();
    ArrayList<Line> axes=new ArrayList<Line>();

  Linegraph(Table data,ArrayList<Integer>useColumns)
  {
    this.useColumns=useColumns;
    this.data=data;
    n=data.getRowCount();
  }
void setPosition(float u0, float v0, float w, float h)
  {
    super.setPosition(u0,v0,w,h);
    }
    void keyPressed()
    {
       if (key == CODED) {
    if (keyCode == RIGHT) {
      
     if(Barx<useColumns.size()-1)
     {
      Barx++;
     }
     else{
     }
    }  if (keyCode == LEFT) {
      if(Barx>0)
      {
        
      Barx--;
    }
      else
      {
      }
  } 
  }
    }
  void updatepositions()
  {
     barwidth=w/n;
    baru0=u0;
    barv0=v0;

       float maxv=max(data.getFloatColumn(Barx));
    float minv=min(data.getFloatColumn(Barx));
   
   for(int j=0;j<data.getRowCount();j++)
    {
      baru0=baru0+barwidth;
      float val=data.getFloat(j,Barx);
      int bv0=(int) map(val,minv,maxv,v0+border, v0+h-border);
     
     // float val=data.getFloat(j,Barx);
       axes.add(new Line(val,baru0,h-bv0,5,5));
       Line line=axes.get(j);
 
    }
   
  }
void draw()
{int s=0;
axes.clear();
updatepositions();
scale();
textSize(20);
stroke(0,0,255);
fill(0,0,255);
text("Line Graph",250,50);
noFill();
   for(Line l:axes)
    {
      
      if(s==0)
      {
     // stroke(0,200,200);
     stroke(0,0,255);
      ellipse(l.u0,l.v0,l.w,l.h);
      }
      else{
        //stroke(204,102,0);
       // stroke(0,0,200);
       stroke(0,0,255);
        ellipse(l.u0,l.v0,l.w,l.h);
        stroke(153);
        if(s!=axes.size()){
        line(axes.get(s-1).u0,axes.get(s-1).v0,axes.get(s).u0,axes.get(s).v0);}
      }
      s++;
    }
     line(u0+barwidth,v0+border,u0+barwidth,v0+h);
     line(u0+barwidth,v0+h,u0+w,v0+h);
     fill(0,0,255);
     textSize(15);
     ellipse(u0+100,v0+h+25,10,10);
    fill(0);
    text(data.getColumnTitle(Barx),u0+175,v0+h+30);
  
}
 void scale()
  {
    stroke(0);
    textSize(10);
    float maxv=max(data.getFloatColumn(Barx));
    float minv=min(data.getFloatColumn(Barx));
    for(int i=0;i<=10;i++)
    {
      float k=i*maxv/10;
      text(String.format("%.1f", k),u0,v0+h-(i*(h-75)/10));
      
    }
    fill(0);
       text("click -->/<-- to change axis",175,h+40);

  }
void mousePressed(){ 
    for( Line l :axes ){
       if( l.mouseInside() ){
       
        println(l.val);
        text(l.val,l.u0,l.v0);
       }
    }
}
void mouseover()
{
  for( Line l :axes ){
       if( l.mouseInside() ){
       
        println(l.val);
        fill(0);
      ellipse(l.u0,l.v0,l.w,l.h);  
        text("value ="+l.val,l.u0,l.v0);
        noFill();
       }
    }
}
}
class Line extends Frame
{
  float val;
  Line(float val,float u0,float v0,float w, float h)
  {
    this.val=val;
    this.u0=u0;
    this.v0=v0;
    this.w=w;
    this.h=h;
  }
  
  void draw()
  {
    
  }
}