
class Scatterplot extends Frame {
   
  
  float minX, maxX;
  float minY, maxY;
  int idx0, idx1;
  int border = 40;
  boolean drawLabels = true;
  float spacer = 5;
  float tempmeanx=0;
  float tempmeany=0;
  float meanx;
  float meany;
  float tempstdx=0;
  float tempstdy=0;
  float stdx;
  float stdy;
  float tempx;
  float tempy;
  TableRow r;
  float tempcov;
  float cov;
  float rc;
  float colour;
  float sze;
  Table data;
 
  
   Scatterplot( Table data, int idx0, int idx1 ){
     
     this.idx0 = idx0;
     this.idx1 = idx1;
     this.data=data;
     
     minX = min(data.getFloatColumn(idx0));
     maxX = max(data.getFloatColumn(idx0));
     
     minY = min(data.getFloatColumn(idx1));
     maxY = max(data.getFloatColumn(idx1));

    
   }
   
   void draw(){
     
      for( int i = 0; i < table.getRowCount(); i++ ){
        
        TableRow r = table.getRow(i);
        tempmeanx=tempmeanx+r.getFloat(idx0);
        tempmeany=tempmeany+r.getFloat(idx1);
        
      }
       meanx=tempmeanx/table.getRowCount();
     meany=tempmeany/table.getRowCount();
     
     for( int i = 0; i < table.getRowCount(); i++ ){
      TableRow  r = table.getRow(i);
       float x=r.getFloat(idx0);
       float y=r.getFloat(idx1);
       tempx=x-meanx;
       tempy=y-meany;
      tempstdx=tempstdx+(tempx*tempx);
       tempstdy=tempstdy+(tempy*tempy);
       stroke(0);
        stroke(255, 0, 102);
        fill(255,0,0);
     }
     stdx=(float)Math.sqrt(tempstdx/table.getRowCount());
     stdy=(float)Math.sqrt(tempstdy/table.getRowCount());
     
     
      for( int i = 0; i < table.getRowCount(); i++ ){
        
        TableRow r = table.getRow(i);
        float x=r.getFloat(idx0);
       float y=r.getFloat(idx1);
       tempcov=tempcov+((x-meanx)*(y-meany));
       
      }
      cov=tempcov/table.getRowCount();
     rc=cov/(stdx*stdy);
     
     //println("mean x        "+rc);
     
          if(rc>=0)
{
     colour=map(rc,0,1,0,255);
}

if(rc<0)
{colour=map(rc,0,-1,0,255);
  
}
     if(rc>=0)
     {
     fill(0,0,colour);
     }
     if(rc<0)
     {
            fill(colour,0,0);

       
     }
    if(idx1!=idx0){
      if((idx0==1 && idx1==0) ||(idx0==2 && idx1==1) ||(idx0==2 && idx1==0) ||(idx0==3 && idx1==0) ||(idx0==3 && idx1==2) || (idx0==3 && idx1==1) )
      {
        ellipse(u0+40,v0+30,map(Math.abs(rc),0,1,10,w-2*border),map(Math.abs(rc),0,1,10,h-2*border));
      }
      else{
     rect( u0+border,v0+border, w-2*border, h-2*border);
      }
    }
     noFill();
     fill(0,255,0);
     textSize(15);
     if(idx0!=idx1){
   text(rc,u0+60,v0+40);}
     
     if( drawLabels ){
       fill(0);
       text( table.getColumnTitle(idx0), u0+width/2, v0+height-10 );
       pushMatrix();
       translate( u0+10, v0+height/2 );
       rotate( PI/2 );
       text( table.getColumnTitle(idx1), 0, 0 );
       popMatrix();
     }
   }
  
}