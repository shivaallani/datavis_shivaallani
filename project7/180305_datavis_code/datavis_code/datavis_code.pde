Frame myFrame = null;
Frame splomFrame=null;
Frame barframe=null;
Frame lineframe=null;
Frame histoframe=null;
Frame spearframe=null;
Table table;

void setup(){
  size(1200,800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        println( i + " - type float" );
        useColumns.add(i);
      }
      else{
        println( i + " - type string" );
      }
    }
    myFrame = new PCP( table, useColumns );
    splomFrame=new Splom(table,useColumns);
    barframe=new Bargraph(table,useColumns);
    lineframe=new Linegraph(table,useColumns);
    histoframe=new Histogram(table,useColumns);
    spearframe=new Spearrankm(table,useColumns);
   
    
  }
}


void draw(){
  if( table == null ) 
    return;
    
    
    
  background(255);
  stroke(0);
    textSize(24);
    //text("DASH BOARD",width/2+30,20);
  stroke(255, 0, 102);
        fill(255,0,102);
//text("Scatter plot",2*(width/3+125),height/2+25);
  fill(0, 102, 153);
 stroke(0,102,153);
  textSize(32);
  line(25,25,1175,25);
  line(25,25,25,775);
  line(1175,25,1175,775);
  line(25,775,1175,775);
  line(25,height/2,1175,height/2);
  line(2*(width/3)+25,25,2*(width/3)+25,775);
 line(width/3,25,width/3,height/2);
// line(2*(width/3),height/2,2*(width/3),1175);
 
  if( myFrame != null ){
       myFrame.setPosition( 50,height/2+50,2*(width/3)-50,height/2-50 );
       myFrame.draw();
       
  }
  if(splomFrame!=null){
    splomFrame.setPosition((2*width/3)+100,0+50,width/4,height/4+50);
    splomFrame.draw();
    splomFrame.zoom();
  }
  
  if(lineframe!=null)
  {
   // lineframe.setPosition(0,height/2,width/3,height/2);
   lineframe.setPosition(50, 0, width/3-50, (height/2)-50);
   // lineframe.draw();
    //lineframe.mouseover();
    
  }
   if(barframe!=null)
  {
    barframe.setPosition((width/3)+50,0,(width/3)-50,(height/2)-50);
  // barframe.draw();
  // barframe.mouseover();
  }
  if(histoframe!=null)
  {
    histoframe.setPosition((width/3)+25,50,(width/3)-50,(height/2)-50);
    histoframe.draw();
    histoframe.mouseover();
  }
  if(spearframe!=null)
  {
    spearframe.setPosition(100, 50, width/4, (height/4)+50);
    spearframe.draw();
    spearframe.zoom();
  }
  
  
  fill(0);
 // noLoop();
  
}
void keyPressed(){
  
barframe.keyPressed();
//lineframe.keyPressed();
histoframe.keyPressed();

}

void mousePressed(){
  myFrame.mousePressed();
  barframe.mousePressed();
 // lineframe.mousePressed();
  splomFrame.mousePressed();
  
}
void mouseReleased(){
  myFrame.mouseReleased();
}

abstract class Frame {
  
 float u0,v0,w,h;
     float clickBuffer = 0;
  void setPosition( float u0, float v0, float w, float h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  
  abstract void draw();
  void mousePressed(){ }
  void keyPressed(){}
  void mouseReleased(){ }
  void zoom(){}
  void mouseover(){}

  
   boolean mouseInside(){
      return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY; 
   }
  
  
}