class Spearrankm extends Frame {
  
    ArrayList<spearrank> plots = new ArrayList<spearrank> ();
    int colCount;
    Table data;
    float border = 20;
    Scatterplotzoom z;
    ArrayList<Integer> useColumns;
    
   Spearrankm( Table data, ArrayList<Integer> useColumns ){
     this.data = data;
     this.useColumns=useColumns;
     colCount = useColumns.size();
         
   /*  for( int j = 0; j < colCount; j++ ){
       for( int i = 0; i <colCount; i++ ){
           Scatterplot sp = new Scatterplot( table, useColumns.get(j), useColumns.get(i) );
           plots.add(sp);
           println("            "+plots.size());
       }
     }*/
       
     
     
   }
   void updatepositions()
   {
     for( int j = 0; j < colCount; j++ ){
       for( int i = 0; i <colCount; i++ ){
           spearrank sp = new spearrank( table, useColumns.get(j), useColumns.get(i) );
           plots.add(sp);
           //println("            "+plots.size());
       }
     }     
     int curPlot = 0;

     for( int j = 0;j <colCount; j++ ){
       for( int i = 0; i <colCount; i++ ){
          spearrank sp = plots.get(curPlot++);
           int su0 = (int)map( i, 1, colCount, u0+border, u0+w-border );
           int sv0 = (int)map( j, 0, colCount-1, v0+border, v0+h-border );
           sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
           sp.drawLabels = false;
           sp.border = 3;

     }
    }
     
   }
   void setPosition( float u0, float v0, float w, float h ){
     super.setPosition(u0,v0,w,h);
         // int curPlot = 0;
   /* for( int j = 0;j <colCount; j++ ){
       for( int i = 0; i <colCount; i++ ){
          Scatterplot sp = plots.get(curPlot++);
           int su0 = (int)map( i, 1, colCount, u0+border, u0+w-border );
           int sv0 = (int)map( j, 0, colCount-1, v0+border, v0+h-border );
           sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
           sp.drawLabels = false;
           sp.border = 3;

     }
    }*/
     
  }

   
   void draw() {
     plots.clear();
     fill(192,0,100);
     textSize(15);
     
     text("Corrogram using Spearman coef Algorithm",u0+w-25,v0);
     fill(255,0,0);
     float ft=8;
     int col=0;
     for(int i=0;i<255;i=i+25)
     {
       col=col+25;
       fill(col,0,0);
       ft=ft+8;
     rect(u0+w-150+ft,v0+h+50,8,8);
     fill(0,0,col);
     rect(u0+w-150+ft,v0+h+75,8,8);
   }
   fill(0);
   text("min",u0+w-140,v0+h+60);
   text("min",u0+w-140,v0+h+85);
   text("max",u0+w,v0+h+60);
   text("max",u0+w,v0+h+85);
   
     
     updatepositions();
     for( spearrank s : plots ){
        s.draw(); 
     }
    stroke(255, 0, 102);
    fill(255,0,102);
    textSize(20);
    //text("Scatter Plot Matrix",2*(width/3)+275,50);
    noFill();
    fill(0);
    stroke(0);
    textSize(11);
    ellipse(2*(width/3)+30,height/2-8,5,5);
    text("Hover over the abox to get the axes of each plot",u0+w-50,v0+h+95);
   }
   
   void zoom()
   {
     for( spearrank sp : plots ){
       if( sp.mouseInside() ){
          // do something!!!
          println(sp.idx0 + " " + sp.idx1);
         // z=new Scatterplotzoom(data, sp);
        //  z.setPosition(2*(width/3)+40,height/2+50,width/3-100,height/2-50);
          //z.draw();
             textSize(15);
          fill(192,0,100);
          text(sp.data.getColumnTitle(sp.idx0)+"  Vs  "+sp.data.getColumnTitle(sp.idx1),u0+140,v0+h+75);
       }
    }
     
   }

  void mousePressed(){ 
    /*for( Scatterplot sp : plots ){
       if( sp.mouseInside() ){
          // do something!!!
          println(sp.idx0 + " " + sp.idx1);
          z=new Scatterplotzoom(data, sp);
          z.setPosition(2*(width/3),height/2,width/3,height/2);
          z.draw();
          
       }
    }*/
  }
}