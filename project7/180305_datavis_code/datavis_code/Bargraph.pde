class Bargraph extends Frame {
      Table data;
    float border=75;
    int Barx; //<>//
    float wid=w-20-20;
    float hei=h-20-20;
    float space;
    int j; 
    float barwidth;
     
    float n;
    float baru0;
    float barv0;
    int temp=0;
    ArrayList<Integer>useColumns=new ArrayList<Integer>();
 
     ArrayList<Bar> axes = new ArrayList<Bar>( );
  
  Bargraph(Table data, ArrayList<Integer> useColumns)
  {
this.data=data;
this.useColumns=useColumns;
 n=data.getRowCount();
// updatepositions();
//updatepositions();
}
  void keyPressed()
  {
    
    if (key == CODED) {
    if (keyCode == RIGHT) {
      
     if(Barx<useColumns.size()-1)
     {
      Barx++;
     }
     else{
     }
    }  if (keyCode == LEFT) {
      if(Barx>0)
      {
        
      Barx--;
    }
      else
      {
      }
  } 
  }
  }
 
  void setPosition(float u0, float v0, float w, float h)
  {
    super.setPosition(u0,v0,w,h);
   /* barwidth=w/n;
    baru0=u0;
    barv0=v0;

       float maxv=max(data.getFloatColumn(Barx));
    float minv=min(data.getFloatColumn(Barx));
   
   for(int j=0;j<data.getRowCount();j++)
    {
      baru0=baru0+barwidth;
      float val=data.getFloat(j,Barx);
      int bv0=(int) map(val,minv,maxv,v0+border, v0+h-border);
       axes.add(new Bar(val,baru0,h-bv0,barwidth,bv0));
       Bar bar=axes.get(j);
    }*/
    

   
    
  }
  void updatepositions()
  {
    
    barwidth=w/n;
    baru0=u0;
    barv0=v0;

       float maxv=max(data.getFloatColumn(Barx));
    float minv=min(data.getFloatColumn(Barx));
   
   for(int j=0;j<data.getRowCount();j++)
    {
      baru0=baru0+barwidth;
      float val=data.getFloat(j,Barx);
      int bv0=(int) map(val,minv,maxv,v0+border, v0+h-border);
      Bar bar=new Bar(val,baru0,h-bv0,barwidth,bv0);
       axes.add(j,bar);
       //Bar bar=axes.get(j);
    }
    
  }
  void scale()
  {
    stroke(0);
    textSize(10);
    float maxv=max(data.getFloatColumn(Barx));
    float minv=min(data.getFloatColumn(Barx));
    for(int i=0;i<=10;i++)
    {
      float k=i*maxv/10;
text(String.format("%.1f", k),u0,v0+h-(i*(h-75)/10));      
    }
   
   text("click -->/<-- to change axis",575,h+40);
    
  }
  void draw()
  {
    axes.clear();
    updatepositions();
    scale();
   stroke(255,255,0);
   fill(255,255,0);
   textSize(20);
    text("Bar Graph",650,50);
    noFill();
   
    for(Bar b:axes)
    {
      //stroke(204, 102, 0);
      stroke(255, 255, 0);
     
      rect(b.u0,b.v0,barwidth,b.h); 
     }
     stroke(0);
     line(u0+barwidth,v0+border,u0+barwidth,v0+h);
     line(u0+barwidth,v0+h,u0+w+barwidth,v0+h);
     fill(255,255,0);
     rect(u0+100,v0+h+10,10,20);
    fill(0);
    textSize(15);
    text(data.getColumnTitle(Barx),u0+175,v0+h+30);
  }
  void mouseover()
  {
     for( Bar b :axes ){
       if( b.mouseInside() ){
       
        println(b.bval);
        textSize(15);
        fill(255);
        rect(b.u0,b.v0,barwidth,b.h); 
        fill(0);
        text("value="+b.bval,b.u0,b.v0);
       }
    }
  }
  void mousePressed(){ 
    for( Bar b :axes ){
       if( b.mouseInside() ){
       
        println(b.bval);
        textSize(10);
        text(b.bval,b.u0,b.v0);
       }
    }
}


}

class Bar extends Frame {

  float val;
  float bval;
  
  Bar(float val,float u0,float v0,float w,float h)
  {
    bval=val;
    this.u0=u0;
    this.v0=v0;
    this.w=w;
    this.h=h;
   
  }
void draw()
{
}
}