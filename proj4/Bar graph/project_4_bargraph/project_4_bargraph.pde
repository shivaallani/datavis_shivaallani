Table data;
int xcol;
int ycol;
ArrayList<Integer> useColumns;
ArrayList<Integer> input= new ArrayList<Integer>();
int i=0;
bargraph b;
boolean newinput=true;
int j=0;
void setup()
  { 
 
  size(800, 600);
  selectInput("Select a file to process:", "fileSelected"); 
 
  while(data==null)
  {
    delay(1000);
  }
  println("chooseone of the below coordinates");
 }
 void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    //selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    data = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for(int i = 0; i < data.getColumnCount(); i++){
      if( !Float.isNaN( data.getRow( 0 ).getFloat(i) ) ){
        println( j + "for" + data.getColumnTitle(i));
        useColumns.add(i);
        j++;
      }
      else{
        println( i + " - type string" );
      }
    }
    
  }
 }
 
 void keyPressed()
 {
   
   if(input.size()<=0)
   {
   int numericValue = Character.getNumericValue(key);
  input.add(numericValue);
  newinput=true;
   }
  if(input.size()>=1)
  {
    newinput=false;
  }
 }
 void draw()
 {
   background(255);
   if(input.size()==1)
   {

     int xcol=input.get(0);
     b=new bargraph(data,xcol);
     b.draw();
      
     
   }
   textSize(15);
   text("Bar Graph", width/3,25);
   delay(1000);
 }
  void mousePressed()
 {
   
   String s=" Xposition"+mouseX;
   String s1="Y position"+mouseY;
   if(get(mouseX,mouseY)!=-1)                      /*sees that popup window doesnot appear when mouse not on graph*/
  {
  rect(500,20,150,100); 
  }
  fill(0);
  text(s,520,50);
  text(s1,520,75);
 }