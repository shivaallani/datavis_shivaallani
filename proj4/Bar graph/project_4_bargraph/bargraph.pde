class bargraph
{
Table  data;
int col;
float axisdata[];
int n=0;
int i=0;
int margin;
int graphheight;
int xspacer;
float overallmin,overallmax;
PVector[] positions;
  bargraph(Table data,int col)
  {
    this.data=data;
    this.col=col;
    n=data.getRowCount();
    axisdata=new float[n];
    positions=new PVector[n];
    for(TableRow row : data.rows())
    {
     
      axisdata[i]=row.getFloat(col);
      i++;
    }
    margin=50;
graphheight=(height-margin)-margin;
xspacer=(width-margin-margin)/(axisdata.length-1);
overallmin= min(axisdata)-12;
overallmax=max(axisdata);
for(int r=0;r<data.getRowCount();r++)
{
  float adjscore=map(axisdata[r],overallmin,overallmax,0,graphheight);
  float ypos=height-margin-adjscore;
  float xpos=margin+(xspacer*r);
  positions[r]=new PVector(xpos,ypos);
}
    
  }
  void draw()
{
   for(int l=0;l<data.getRowCount();l++)
  {
   
   
    
    rect(positions[l].x,positions[l].y,15,height-positions[l].y-margin);
  }
  line(margin,height-margin,width-margin,height-margin);
  line(margin,height-margin,margin,margin);
  float normfactor=(overallmax-overallmin)/10;
    int c =(height-2*margin)/10;
    for(int j=0;j<10;j++)
  
  {
    noStroke();
fill(0,126,255);
    textSize(10);
    String S=String.format("%.2f",overallmin+(normfactor*j));
    text(S,margin-50,(height-margin)-(c*j));
   stroke(100);
    line(margin,(height-margin)-(c*j),width-margin,(height-margin)-(c*j));
  }
 textSize(15);
 text(data.getColumnTitle(xcol),width/3,height-25);
}
}