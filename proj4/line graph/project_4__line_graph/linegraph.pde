class linegraph
{
  int i=0;
  Table data;
  float xaxismin;
  float yaxismin;
  String xaxistitle;
  String yaxistitle;
  int col;
  int n;
  float axisdata[];
  float overallmin;
float overallmax;
float xspacer;
int margin,graphheight;
PVector[] positions;

  linegraph(Table data,int col)
  {
    this.data=data;
    this.col=col;
     n=data.getRowCount();
    axisdata=new float[n];
    positions=new PVector[n];
     
     for (TableRow row : data.rows()) {
 
  axisdata[i]=row.getFloat(col);  
    i++;
  }
  margin=50;
graphheight=(height-margin)-margin;
xspacer=(width-margin-margin)/(axisdata.length-1);
overallmin= min(axisdata)-12;
overallmax=max(axisdata);
for(int r=0;r<data.getRowCount();r++)
{
  float adjscore=map(axisdata[r],overallmin,overallmax,0,graphheight);
 
  float ypos=height-margin-adjscore;
  float xpos=margin+(xspacer*r);
  //println(ypos);
  
  positions[r]=new PVector(xpos,ypos);
}
    
    
  }
  void draw()
  {
  
   
    for(int r=0; r<data.getRowCount();r++)
  {
  
    
    
    ellipse(positions[r].x,positions[r].y,5,5);
    
  }
  for(int l=0;l<data.getRowCount();l++)
  {
    
    if(l>0)
    {
      stroke(100);
      line(positions[l].x,positions[l].y,positions[l-1].x,positions[l-1].y);
    }
    
  }
  line(margin,height-margin,width-margin,height-margin);
  line(margin,height-margin,margin,margin);
  textSize(32);
  text(data.getColumnTitle(col),width/2-25,25);
  textSize(10);
    float normfactor=(overallmax-overallmin)/10;
    int c =(height-2*margin)/10;
    for(int j=0;j<10;j++)
  
  {
    noStroke();
fill(0,126,255);
    textSize(10);
    String S=String.format("%.2f",overallmin+normfactor*j);
    text(S,margin-50,(height-margin)-(c*j));
   stroke(100);
    line(margin,(height-margin)-(c*j),width-margin,(height-margin)-(c*j));
  }
    
    
  }
}