class Scatterplot
 {
   Table data;
   int xcol;
   int ycol;
   float[] xaxisdata;
float[] yaxisdata;
float n;
int i=0,j=0;
int margin,graphheight;
float xspacer;
PVector[] positions;
float xaxismin;
float xaxismax;
float yaxismin;
float yaxismax;
String xaxistitle;
String yaxistitle;
  Scatterplot(Table data,int xcol,int ycol)
  {
    this.data=data;
    this.xcol=xcol;
    this.ycol=ycol;
    int n=data.getRowCount();
    yaxisdata=new float[n];
    xaxisdata=new float[n];
    positions=new PVector[n];
     for (TableRow row : data.rows()) {

  xaxisdata[i]=row.getFloat(xcol);
  yaxisdata[i]=row.getFloat(ycol);
  
    i++;
  }
   margin=100;
graphheight=(height-margin)-margin;
//xspacer=(width-margin-margin)/(table.getRowCount()-1);
xaxismin= min(xaxisdata);
xaxismax=max(xaxisdata);
yaxismax=max(yaxisdata);
yaxismin=min(yaxisdata);


for(int r=0;r<data.getRowCount();r++)
{
  float yadjscore=map(yaxisdata[r],yaxismin,yaxismax,0,graphheight);
  float ypos=height-margin-yadjscore;
  float xadjscore=map(xaxisdata[r],xaxismin,xaxismax,0,width-margin-margin);
  //println(xadjscore);
  float xpos=margin+xadjscore;
  positions[r]=new PVector(xpos,ypos);
}
    
  }
  
  
  void draw()
  {
    
   for (i=0;i<data.getRowCount();i++) 
   {
     //fill(255);
   ellipse(positions[i].x,positions[i].y,3,3);
   }  
     drawlabels();
   drawaxis();
    
  }
  void drawlabels()
  {
    int c=(height-2*margin)/10;
  int d=(width-2*margin)/10;
  float xnormfactor=(xaxismax-xaxismin)/10;
  float ynormfactor=(yaxismax-yaxismin)/10;
  for(int j=0;j<10;j++)
  // labeling for x axis
  {
    noStroke();
fill(0,126,255);
    textSize(10);
    String S=String.format("%.2f",yaxismin+(ynormfactor*j));
    text(S,margin-50,(height-margin)-(c*j));
   stroke(100);
    line(margin,(height-margin)-(c*j),width-margin,(height-margin)-(c*j));
  }
  //labeling for y axis
for(int k=0;k<10;k++)
{  noStroke();
 fill(0,126,255);
     String L=String.format("%.2f",xaxismin+(xnormfactor*k));
   text(L,margin+(d*k),(height-margin+25));
   stroke(100);
   line(margin+(d*k),height-margin,margin+(d*k),margin);
   
}
//drawing head title
fill(127,0,0);
textSize(32);
xaxistitle=data.getColumnTitle(xcol);
yaxistitle=data.getColumnTitle(ycol);
text(xaxistitle +"  Vs  "+yaxistitle,width/4,margin/2);
textSize(15);
text(xaxistitle,width/2,550);
text(yaxistitle,15,height/2+25);
    
  }
  void drawaxis()
  {
    //draw X and Y axis
  stroke(0);
  line(margin,height-margin,width-margin,height-margin);
  line(margin,height-margin,margin,margin);
    
  }
  
 }