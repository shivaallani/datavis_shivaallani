

class Splom extends Frame {
  
    ArrayList<Scatterplot> plots = new ArrayList<Scatterplot>( );
    int colCount;
    Table data;
    float border = 20;
    
   Splom( Table data, ArrayList<Integer> useColumns ){
     this.data = data;
     colCount = useColumns.size();
     for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
           Scatterplot sp = new Scatterplot( table, useColumns.get(j), useColumns.get(i) );
           plots.add(sp);
       }
     }
       
     
     //table.getColumnCount()
     //table.getColumnType(int column) != Table.STRING
     //table.getColumnTitle();
     
   }
   
   void setPosition( int u0, int v0, int w, int h ){
     super.setPosition(u0,v0,w,h);

    int curPlot = 0;
    for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
          Scatterplot sp = plots.get(curPlot++);
           int su0 = (int)map( i, 1, colCount, u0+border, u0+w-border );
           int sv0 = (int)map( j, 0, colCount-1, v0+border, v0+h-border );
           sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
           sp.drawLabels = false;
           sp.border = 3;
     }
    }
     
  }

   
   void draw() {
     for( Scatterplot s : plots ){
        s.draw(); 
     }
     textSize(15);
     text("Scatter plot Matrix",width/3,10);
   }
   

  void mousePressed(){ 
    for( Scatterplot sp : plots ){
       if( sp.mouseInside() ){
          // do something!!!
          
          fill(0);
          rect(100,100,300,300);
          
          
          float minx=sp.minX;
          float miny=sp.minY;
          float maxx=sp.maxX;
          float maxy=sp.maxY;
          int x=sp.idx0;
          int y=sp.idx1;
          for( int i = 0; i < table.getRowCount(); i++ ){
        TableRow r = table.getRow(i);
        
        float x1 = map( r.getFloat(x), minx, maxx, 100, 400 );
        float y1 = map( r.getFloat(y), miny, maxy, 100, 400 );
       
          
        pushMatrix();
        translate(300,300);
        rotate(PI/2);
        popMatrix();
        stroke( 0 );
        fill(255,0,0);
        ellipse( x1,y1,3,3 );
        
     }
          
          println(sp.idx0 + " " + sp.idx1);
       }
    }
  }
}