File file;
String[] rawdata;
Table table;
int[] yearstr;
float[] val0str;
float[] val1str;
String[] partystr;
int n=0;
int i=0,j=0;
float xpos, ypos, xwidth=30;
int margin,graphheight;
float xspacer;
PVector[] positions;
float overallmin;
float overallmax;



void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    table = loadTable(selection.getName(), "header");

    int n=table.getRowCount();
    yearstr=new int[n];
    val0str=new float[n];
    val1str=new float[n];
    partystr=new String[n];
    positions=new PVector[n];

    loaddata();
  }
}

void loaddata()
{


  for (TableRow row : table.rows()) {

    int year=row.getInt("YEAR");
    //print(year);
    yearstr[i]=year;
    float val0=row.getFloat("VALUE0");
    val0str[i]=val0;
    float val1=row.getFloat("VALUE1");
    val1str[i]=val1;
    String party=row.getString("PARTY");
    partystr[i]=party;
    i++;
  }
  margin=50;
graphheight=(height-margin)-margin;
xspacer=(width-margin-margin)/(yearstr.length-1);
overallmin= min(val1str)-12;
overallmax=max(val1str);
println(overallmin);
println(overallmax);
for(int r=0;r<table.getRowCount();r++)
{
  float adjscore=map(val1str[r],overallmin,overallmax,0,graphheight);
  float ypos=height-margin-adjscore;
  float xpos=margin+(xspacer*r);
  positions[r]=new PVector(xpos,ypos);
}
}


void setup() {

  size(800, 300);
 // surface.setResizable(true);
  selectInput("Select a file to process:", "fileSelected");
  while(table==null){
  delay(1000);
  }
}
void draw()
{
  background(20);
  fill(200);
  drawGUI();
  
 
  for(int l=0;l<table.getRowCount();l++)
  {
    if(partystr[l].equals ("DEM"))
    {
    fill(127,0,0);
    rect(700,20,15,15);
    text("DEM",720,30);
    }
    if(partystr[l].equals("REP"))
    {
   
      fill(0,126,255);
      rect(700,40,15,15);
      text("REP",720,50);
    }
   
   // ellipse(positions[l].x,positions[l].y,15,15);
    rect(positions[l].x,positions[l].y,15,height-positions[l].y-margin);
    ellipse(positions[l].x,positions[l].y,15,15);
  }
  fill(127,0,0);
  
  /* for(int l=0;l<table.getRowCount();l++)
  {
   
    ellipse(positions[l].x,positions[l].y,15,15);
    //rect(positions[l].x,positions[l].y,15,height-positions[l].y-margin);
  }*/
  color c = color(0, 126, 255);  // Define color 'c'
fill(c);
  line(50,250,800,250);
   text("YEAR",400,290);
  //line(50,250,50,50);
  text("V",25,100);
  text("A",25,110);
  text("L",25,120);
  text("U",25,130);
  text("E",25,140);
  text("1",25,160);
  text(overallmax,5,margin);
  text(overallmin,5,height-margin);
}

void drawGUI()
{
  for(int l=0;l<table.getRowCount();l++)
  {
    stroke(200,100);
     //line(positions[l].x,margin,positions[l].x,height-margin);
    text(yearstr[l],positions[l].x-15,height-margin+20);
    if(l>0)
    {
      stroke(200);
      line(positions[l].x,positions[l].y,positions[l-1].x,positions[l-1].y);
    }
    
  }
  
  
}