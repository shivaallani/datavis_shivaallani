class Splom extends Frame {
  
    ArrayList<Scatterplot> plots = new ArrayList<Scatterplot>( );
    int colCount;
    Table data;
    float border = 20;
    Scatterplotzoom z;
    
   Splom( Table data, ArrayList<Integer> useColumns ){
     this.data = data;
     colCount = useColumns.size();
     for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
           Scatterplot sp = new Scatterplot( table, useColumns.get(j), useColumns.get(i) );
           plots.add(sp);
       }
     }
       
     
     
   }
   void setPosition( float u0, float v0, float w, float h ){
     super.setPosition(u0,v0,w,h);

    int curPlot = 0;
    for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
          Scatterplot sp = plots.get(curPlot++);
           int su0 = (int)map( i, 1, colCount, u0+border, u0+w-border );
           int sv0 = (int)map( j, 0, colCount-1, v0+border, v0+h-border );
           sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
           sp.drawLabels = false;
           sp.border = 3;
     }
    }
     
  }

   
   void draw() {
     for( Scatterplot s : plots ){
        s.draw(); 
     }
    stroke(255, 0, 102);
    fill(255,0,102);
    textSize(20);
    text("Scatter Plot Matrix",2*(width/3)+275,50);
    noFill();
    fill(0);
    stroke(0);
    textSize(11);
    ellipse(2*(width/3)+30,height/2-8,5,5);
    text("Hover over the abox to get the detailed view in below block",width-45,height/2-5);
   }
   
   void zoom()
   {
     for( Scatterplot sp : plots ){
       if( sp.mouseInside() ){
          // do something!!!
          println(sp.idx0 + " " + sp.idx1);
          z=new Scatterplotzoom(data, sp);
          z.setPosition(2*(width/3)+40,height/2+50,width/3-100,height/2-50);
          z.draw();
          
       }
    }
     
   }

  void mousePressed(){ 
    /*for( Scatterplot sp : plots ){
       if( sp.mouseInside() ){
          // do something!!!
          println(sp.idx0 + " " + sp.idx1);
          z=new Scatterplotzoom(data, sp);
          z.setPosition(2*(width/3),height/2,width/3,height/2);
          z.draw();
          
       }
    }*/
  }
}